﻿namespace SMS.DirectSupply.Domain
{
    using System;

    public class IBANNotValidException : Exception
    {
        public IBANNotValidException(string value)
            :base($"Vermiş olduğunuz {value} nolu iban hatalıdır")
        {

        }
    }
}