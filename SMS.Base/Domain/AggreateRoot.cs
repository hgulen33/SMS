﻿namespace SMS.Base.Domain
{
    using System;

    public abstract class AggreateRoot : Entity
    {
        //private IEventPublisher eventPublisher = new EventPublisher();
    }

    public abstract class Entity
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; protected set; }
        //public EntityStatus Status { get; private set; }

        //public void MarkAsRemoved()
        //{
        //    Status = EntityStatus.Deleted;
        //}

        //public void MarkAsAdded()
        //{
        //    Status = EntityStatus.Added;
        //}
        //public void MarkAsModified()
        //{
        //    Status = EntityStatus.Modified;
        //}

        public override bool Equals(object obj)
        {
            var compareTo = obj as Entity;

            if (ReferenceEquals(compareTo, null))
                return false;

            if (ReferenceEquals(this, compareTo))
                return true;

            //if (GetRealType() != compareTo.GetRealType())
            //    return false;

            return !this.IsTransient() && !compareTo.IsTransient() && this.Id == compareTo.Id;
        }

        public static bool operator ==(Entity a, Entity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Entity a, Entity b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return GetType().GetHashCode() * 19 + Id.GetHashCode();
        }

        public virtual bool IsTransient()
        {
            return Id == Guid.Empty;
        }
    }

    public enum EntityStatus
    {
        Added = 0,
        Modified = 1,
        Deleted = 2,
        Unchanged = 4
    }

    public abstract class ValueObject<T>
    where T : ValueObject<T>
    {
        public override bool Equals(object obj)
        {
            var valueObject = obj as T;

            return !ReferenceEquals(valueObject, null) && this.EqualsCore(valueObject);
        }

        protected abstract bool EqualsCore(T other);

        public override int GetHashCode()
        {
            return GetHashCodeCore();
        }

        protected abstract int GetHashCodeCore();

        public static bool operator ==(ValueObject<T> a, ValueObject<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(ValueObject<T> a, ValueObject<T> b)
        {
            return !(a == b);
        }
    }


}