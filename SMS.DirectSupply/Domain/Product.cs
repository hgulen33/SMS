﻿namespace SMS.DirectSupply.Domain
{
    using System;
    using System.Collections.Generic;

    using FluentNHibernate.Testing.Values;

    using SMS.Base.Domain;

    public class Product : Entity
    {
        public Product()
        {
            this.Id = Guid.NewGuid();
        }

        public Product(string name, string desc)
        {
            Name = name;
            Description = desc;
            //Quantity = quantity;
            //UnitPrice = price;
        }

        public Product(Guid guid, string name, string desc)
        {
            this.Id = guid;
            this.Name = name;
            this.Description = desc;
            //this.Quantity = quantity;
            //this.UnitPrice = price;
        }

        public virtual string Name { get; protected set; }

        public virtual string Description { get; protected set; }

        //public virtual Quantity Quantity { get; protected set; }

        //public virtual Money UnitPrice { get; protected set; }

        //public virtual Money TotalPrice => new Money((decimal)this.Quantity.Value * this.UnitPrice.Amount);



    }

    //public class Customer
    //{
    //    public int Id { get; set; }

    //    public string Name { get; set; }

    //    public string Address1 { get; set; }
    //    public string Address2 { get; set; }

    //    public List<Order> Orders { get; set; }
    //}

    //public class Order
    //{
    //    public int Id { get; set; }

    //    public int CustomerId { get; set; }

    //    public DateTime OrderDate { get; set; }

    //    public List<OrderItem> Products { get; set; }
    //}

    //public class OrderItem
    //{
    //    public int Id { get; set; }

    //    public string Name { get; set; }

    //    public Prod Prod { get; set; }

    //    public decimal Miktar { get; set; }

    //    public decimal LineTotal
    //    {
    //        get
    //        {
    //           return  Miktar * Prod.Fiyat;
    //        }
    //    }
    //}

    //public class Prod
    //{
    //    public decimal Fiyat { get; set; }
    //}

}