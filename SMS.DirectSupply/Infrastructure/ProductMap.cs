﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.DirectSupply.Infrastructure
{
    using FluentNHibernate.Mapping;

    using Domain;

    public class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            this.Id(product => product.Id);
            this.Map(product => product.Id)
                .Generated.Never()
                .Not.Nullable();

            this.Map(product => product.Name)
                .Not.Nullable()
                .Length(500);

            this.Map(product => product.Description)
                .Nullable()
                .Length(1000);

            //this.Component(product => product.Quantity);
            //this.Component(product => product.UnitPrice);


        }
    }

    public class MoneyMap : ComponentMap<Money>
    {
        public MoneyMap()
        {
            this.Map(money => money.Amount)
                .Precision(2);
        }
    }

    public class QuantityMap : ComponentMap<Quantity>
    {
        public QuantityMap()
        {
            this.Map(money => money.Value)
                .Precision(2);
            this.Component(money => money.Unit);
        }
    }

    public class UnitMap : ComponentMap<Unit>
    {
        public UnitMap()
        {
            this.Map(unit => unit.Name)
                .Length(10);
        }
    }
}
