﻿namespace SMS.DirectSupply.Domain
{
    using System;

    using SMS.Base.Domain;

    public class Unit : ValueObject<Unit>
    {
        public Unit(string name)
        {
            this.Name = name;
        }

        public virtual string Name { get; }

        protected override bool EqualsCore(Unit other)
        {
            return this.Name == other.Name;
        }

        protected override int GetHashCodeCore()
        {
            return (this.Name.GetHashCode() * 19) ^ 397;
        }
    }

    public class UnitsNotSameException : Exception
    {
        public UnitsNotSameException()
            : base("Birimler aynı değil")
        {

        }
    }
}