﻿namespace SMS.DirectSupply.Domain
{
    using System;

    using SMS.Base.Domain;

    public class Supplier : Entity
    {

        public Supplier()
        {
        }

        public Supplier(string name, string phone, Guid supplierId)
        {
            this.Name = name;
            this.Phone = phone;
            this.Id = supplierId;
        }

        public Supplier(string name, string phone)
        {
            this.Name = name;
            this.Phone = phone;
            Id = Guid.NewGuid();
        }

        public Guid SupplierId { get; protected set; }

        public string Name { get; protected set; }
        public string Phone { get; protected set; }

        public IBAN IBAN { get; protected set; }
    }
}