﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMS.DirectSupply.Test
{
    using System.Diagnostics;

    using FluentAssertions;

    using NUnit.Framework;

    using Domain;

    [TestFixture]
    public class MoneyTestClass
    {
        [Test]
        public void AddingMoneyTest()
        {
            var m1 = new Money(100);
            var m2 = new Money(200);

            (m1 + m2).Should()
                     .Be(new Money(300));
        }

        [Test]
        public void SubMoneyTest()
        {
            var m1 = new Money(100);
            var m2 = new Money(200);

            (m1 - m2).Should()
                     .Be(new Money(-100));
        }

        [Test]
        public void MultiplyMoneyByMoneyTest()
        {
            var m1 = new Money(100);
            var m2 = new Money(20);

            (m1 * m2).Should()
                     .Be(new Money(2000));
        }

        [Test]
        public void MultiplyMoneyByDecimalTest()
        {
            var m1 = new Money(100);
            const double M2 = 20.5;

            (m1 * (decimal)M2).Should()
                     .Be(new Money(2050));
        }

        [Test]
        public void DivisionMoneyByDecimalTest()
        {
            var m1 = new Money(100);
            const int M2 = 20;
            (m1 / M2).Should()
                     .Be(new Money(5));
        }

        [Test]
        public void Test()
        {
            var m1 = new Money(100.25M);
            m1.ToString()
              .Should()
              .NotBeNullOrEmpty();
            Debug.WriteLine(m1);
        }
    }
}
