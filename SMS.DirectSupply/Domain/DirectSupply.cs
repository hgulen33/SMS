﻿namespace SMS.DirectSupply.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using SMS.Base.Domain;

    public class DirectSupply : AggreateRoot
    {

        public DirectSupply()
        {
            this.Id = Guid.NewGuid();
            this.Products =  new List<Product>();
            this.Bids = new List<Bid>();
            this.UnitPriceDemands = new List<UnitPriceDemand>();
        }


        public DirectSupply(Guid id)
        {
            this.Id = id;
            this.Products =  new List<Product>();
            this.Bids = new List<Bid>();
        }

        public virtual List<Product> Products { get; protected set; }
        public virtual List<Bid> Bids { get; protected set; }
        public virtual List<UnitPriceDemand> UnitPriceDemands { get; protected set; }

        //public Guid AddBid()

        public Guid AddProduct(string name, string desc)
        {
            var prod = new Product(name, desc);
            this.Products.Add(prod);
            return prod.Id;
        }

        public void RemoveProduct(Guid id)
        {
            var prod = this.Products.FirstOrDefault(demand => demand.Id == id);
            if (prod != null && Products.Contains(prod))
                Products.Remove(prod);
        }

        public Guid AddUnitPriceDemand(Guid supplierId, Guid productId, Money price)
        {
            var unitPriceDemand = new UnitPriceDemand(supplierId, productId, price);
            this.UnitPriceDemands.Add(unitPriceDemand);
            return unitPriceDemand.Id;
        }

        public void RemoveUnitPriceDemand(Guid id)
        {
            var unitPriceDemand = this.UnitPriceDemands.FirstOrDefault(demand => demand.Id == id);
            if (unitPriceDemand != null && UnitPriceDemands.Contains(unitPriceDemand)) UnitPriceDemands.Remove(unitPriceDemand);
        }

        public void SetWinnerBid(WiningBid bid)
        {
            this.WiningBid = bid;
        }

        public virtual WiningBid WiningBid { get; protected set; }
    }


    public class Comission : Entity
    {
        /// <summary>
        /// Komisyon adı
        /// </summary>
        public string Name { get; set; }

        public List<User> Users { get; set; }
    }

    public class User : Entity
    {
        /// <summary>
        /// Ad
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Soyad
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Unvan
        /// </summary>
        public string Title { get; set; }
    }

    /// <summary>
    /// Domain service, Domain servisi
    /// </summary>
    public class BidCalculator
    {
        public WiningBid CalculateWinnerBid(List<Bid> bids)
        {
            var winnderBid = bids.OrderByDescending(bid => bid.CalculateTotalBid())
                            .First();
            var winingBid = new WiningBid(winnderBid.SupplierId, winnderBid.CalculateTotalBid());
            return winingBid;
        }
    }
}