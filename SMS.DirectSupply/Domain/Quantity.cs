﻿namespace SMS.DirectSupply.Domain
{
    using System;

    using SMS.Base.Domain;

    public class Quantity : ValueObject<Quantity>
    {
        public Quantity(double value, Unit unit)
        {
            this.Value = value;
            this.Unit = unit;
        }

        public virtual double Value { get; }

        public virtual Unit Unit { get; }

        protected override bool EqualsCore(Quantity other)
        {
            return Math.Abs(this.Value - other.Value) < 0.001 && this.Unit == other.Unit;
        }

        protected override int GetHashCodeCore()
        {
            return (this.Value.GetHashCode() * 17) ^ this.Unit.GetHashCode();
        }

        public static Quantity operator +(Quantity leftQuantity, Quantity rightQuantity)
        {
            if (leftQuantity.Unit == rightQuantity.Unit)
                return new Quantity(leftQuantity.Value + rightQuantity.Value, new Unit(leftQuantity.Unit.Name));
            throw new UnitsNotSameException();
        }

        public static Quantity operator -(Quantity leftQuantity, Quantity rightQuantity)
        {
            if (leftQuantity.Unit == rightQuantity.Unit) return new Quantity(leftQuantity.Value - rightQuantity.Value, new Unit(leftQuantity.Unit.Name));
            throw new UnitsNotSameException();
        }

        public Quantity Add(double value)
        {
            var res = this.Value + value;
            if (res >= 0)
                return new Quantity(res, this.Unit);
            throw new QuantityValueExeption("Miktar sıfır ya da sıfırdan büyük olmalıdır.");
        }

        public Quantity Substract(double value)
        {
            var res = this.Value - value;
            if (res >= 0)
                return new Quantity(res, this.Unit);
            throw new QuantityValueExeption("Miktar sıfır ya da sıfırdan büyük olmalıdır.");
        }

        public Quantity Multiply(double value)
        {
            var res = this.Value * value;
            if (res >= 0)
                return new Quantity(res, this.Unit);
            throw new QuantityValueExeption("Miktar sıfır ya da sıfırdan büyük olmalıdır.");
        }

        public Quantity Divide(double value)
        {
            if (value > 0)
            {
                var res = this.Value / value;
                return new Quantity(res, this.Unit);
            }
            throw new DivideByZeroException("Miktar sıfır ya da sıfırdan büyük olmalıdır.");
        }
    }
}