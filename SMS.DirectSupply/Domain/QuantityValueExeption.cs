﻿using System;

namespace SMS.DirectSupply.Domain
{
    public class QuantityValueExeption : Exception
    {
        public QuantityValueExeption(string message)
            : base(message)
        {
            
        }
    }


}