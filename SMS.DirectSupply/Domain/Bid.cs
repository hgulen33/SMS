﻿namespace SMS.DirectSupply.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Base.Domain;

    public class Bid : AggreateRoot
    {
        public Bid()
        {
            BidItems = new List<BidItem>();
            //Products = new ObservableCollection<Product>();
        }

        public Bid(Guid id)
        {
            this.Id = Guid.NewGuid();
            this.DirectSupplyId = id;
        }

        public virtual Guid SupplierId { get; protected set; }

        /// <summary>
        /// Diğer root un referansını tutuyor
        /// </summary>
        public virtual Guid DirectSupplyId { get; protected set; }

        public virtual DateTime BidDate { get; protected set; }

        ///// <summary>
        ///// Bu özellik eşlenmeyecek veri tabanına
        ///// </summary>
        //public  IList<Product> Products { get; protected set; }

        //public void AddProducts(IList<Product> products)
        //{
        //    if (Products == null) Products = new ObservableCollection<Product>(products);
        //    else
        //    {
        //        Products.Clear();
        //        foreach (var p in products)
        //        {
        //            Products.Add(p);
        //        }
        //    }
        //}

        public Money CalculateTotalBid()
        {
            var total = new Money(0);
            return this.BidItems.Aggregate(total, (current, bidItem) => current + bidItem.LineTotal);
        }

        public void SetDirectSupply(Guid id)
        {
            if (id == Guid.Empty) throw new IdentityNotNullableException();
            DirectSupplyId = id;
        }

        public void ChangeSupplier(Guid supplierId)
        {
            if (supplierId != Guid.Empty)
                this.SupplierId = supplierId;
            else
            {
                throw new IdentityNotNullableException();
            }
        }


        public void AddBidItem(BidItem item)
        {
            this.BidItems?.Add(item);
        }

        public void RemoveBidItem(BidItem item)
        {
            BidItems?.Remove(item);
        }

        public IList<BidItem> BidItems { get; protected set; }
    }

    public class BidItem : Entity
    {
        public BidItem()
        {
            
        }

        public BidItem(Guid productId, Quantity quantity, Money unitPrice)
        {
            if (productId == Guid.Empty) throw new IdentityNotNullableException();
            ProductId = productId;
            Quantity = quantity;
            UnitPrice = unitPrice;

        }

        public virtual Quantity Quantity { get; protected set; }
        public virtual Guid ProductId { get;protected set; }
        public virtual Money UnitPrice { get; protected set; }

        public Money LineTotal => new Money(this.UnitPrice.Amount * (decimal)this.Quantity.Value);
    }
}