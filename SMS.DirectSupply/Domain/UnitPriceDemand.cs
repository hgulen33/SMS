namespace SMS.DirectSupply.Domain
{
    using System;

    using SMS.Base.Domain;

    public class UnitPriceDemand : Entity
    {
        public UnitPriceDemand()
        {
            
        }

        public UnitPriceDemand(Guid supplierId, Guid productId, Money unitPrice)
        {
            if (supplierId == Guid.Empty || productId == Guid.Empty)
                throw new IdentityNotNullableException();
            if (unitPrice <= Money.MoneyFrom(0)) throw new MoneyValueException();
            this.SupplierId = supplierId;
            this.UnitPrice = unitPrice;
            this.ProductId = productId;
        }

        public virtual Money UnitPrice { get; protected set; }
        public virtual Guid ProductId { get; protected set; }

        public virtual Guid SupplierId { get; protected set; }
    }

    public class MoneyValueException : Exception
    {
        public MoneyValueException()
            : base("Para de�eri 0 ve s�f�rdan k���k olamaz")
        {
            
        }
    }
}