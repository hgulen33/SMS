namespace SMS.DirectSupply.Domain
{
    using System.Text;

    using SMS.Base.Domain;

    public class IBAN : ValueObject<IBAN>
    {
        public string Value { get; private set; }

        public IBAN(string iban)
        {
            if (Validate(iban)) this.Value = iban;
            else
                throw new IBANNotValidException(this.Value); 
        }

        private static bool Validate(string iban)
        {
            if (string.IsNullOrEmpty(iban))
                return false;

            var asciiShift = 55;
            iban = iban.ToUpper();

            if (!System.Text.RegularExpressions.Regex.IsMatch(iban, "^[A-Z0-9]")) return false;

            //Rearrange iban
            iban = iban.Replace(" ", string.Empty);
            var rearrangedIban = iban.Substring(4, iban.Length - 4) + iban.Substring(0, 4);

            //Convert to integer checksum
            var stringBuilder = new StringBuilder();
            foreach (var ibanChar in rearrangedIban)
            {
                int convertedValue;
                if (char.IsLetter(ibanChar))
                    convertedValue = ibanChar - asciiShift;
                else
                    convertedValue = int.Parse(ibanChar.ToString());
                stringBuilder.Append(convertedValue);
            }

            //Modulo operation on IBAN checksum
            var checkSumString = stringBuilder.ToString();
            var checksum = int.Parse(checkSumString.Substring(0, 1));
            for (var i = 1; i < checkSumString.Length; i++)
            {
                checksum *= 10;
                checksum += int.Parse(checkSumString.Substring(i, 1));
                checksum %= 97;
            }
            return checksum == 1;
        }

        protected override bool EqualsCore(IBAN other)
        {
            return this.Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            return (this.Value.GetHashCode() * 29) ^ 397;
        }

        public override string ToString()
        {
            return this.Value;
        }
    }
}