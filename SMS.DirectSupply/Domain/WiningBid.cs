namespace SMS.DirectSupply.Domain
{
    using System;

    using SMS.Base.Domain;

    public class WiningBid : ValueObject<WiningBid>
    {
        public WiningBid(Guid supplierId, Money total)
        {
            this.SupplierId = supplierId;
            //this.Supplier = supplierName;
            this.Total = total;
        }

        public Money Total { get; protected set; }

        //public string Supplier { get; protected set; }

        public Guid SupplierId { get; protected set; }

        protected override bool EqualsCore(WiningBid other)
        {
            return other.Total == this.Total 
                   //&& other.Supplier == Supplier 
                   && other.SupplierId == this.SupplierId;
        }

        protected override int GetHashCodeCore()
        {
            return (this.SupplierId.GetHashCode() * 29) / 11;
        }
    }
}