﻿namespace SMS.DirectSupply.Domain
{
    using System;

    public class IdentityNotNullableException
        : Exception
    {
        public IdentityNotNullableException()
            : base("Id değerleri boş geçilemez.")
        {
            
        }
    }
}