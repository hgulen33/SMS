﻿namespace SMS.DirectSupply.Infrastructure
{
    using FluentNHibernate.Mapping;

    using SMS.DirectSupply.Domain;

    public class DirectSupplyMap : ClassMap<DirectSupply>
    {
        public DirectSupplyMap()
        {
            this.Id(supply => supply.Id);
            this.Map(supply => supply.Id)
                .Generated.Never()
                .Not.Nullable();

            this.Map(supply => supply.WiningBid);
            this.HasMany(supply => supply.Products)
                .Cascade.AllDeleteOrphan()
                .Fetch.Select()
                .Not.LazyLoad();
        }
    }

    public class BidMap : ClassMap<Bid>
    {
        public BidMap()
        {
            this.Id(bid => bid.Id);
            this.Map(bid => bid.Id)
                .Generated.Never()
                .Not.Nullable();

            this.Map(bid => bid.BidDate)
                .Not.Nullable();

            this.Map(bid => bid.DirectSupplyId)
                .Not.Nullable();

            this.HasMany(bid => bid.BidItems)
                .Cascade.AllDeleteOrphan()
                .Fetch.Select()
                .Not.LazyLoad();
        }
    }

    public class BidItemMap : ClassMap<BidItem>
    {
        public BidItemMap()
        {
            this.Id(item => item.Id);
            this.Map(item => item.Id)
                .Not.Nullable()
                .Generated.Never();

            this.Map(item => item.ProductId)
                .Not.Nullable();

            this.Map(item => item.UnitPrice);
            this.Map(item => item.Quantity);
        }
    }

    public class UnitPriceDemandMap : ClassMap<UnitPriceDemand>
    {
        public UnitPriceDemandMap()
        {
            this.Id(demand => demand.Id);
            this.Map(demand => demand.Id)
                .Not.Nullable()
                .Generated.Never();

            this.Map(demand => demand.ProductId);
            this.Map(demand => demand.SupplierId);
            this.Map(demand => demand.UnitPrice);
        }
    }


}