﻿namespace SMS.DirectSupply.Domain
{
    using SMS.Base.Domain;

    public class Money : ValueObject<Money>
    {
        public virtual decimal Amount { get; }

        public Money(decimal amount)
        {
            this.Amount = amount;
        }

        public override string ToString()
        {
            return $"{this.Amount} ₺";
        }

        protected override bool EqualsCore(Money other)
        {
            return this.Amount == other.Amount;
        }

        protected override int GetHashCodeCore()
        {
            return (this.Amount.GetHashCode() * 17) ^ 397;
        }

        public static Money MoneyFrom(decimal value)
        {
            return new Money(value);
        }

        public static Money operator +(Money leftMoney, Money rightMoney)
        {
            return new Money(leftMoney.Amount + rightMoney.Amount);
        }

        public static Money operator +(Money leftMoney, decimal rightMoney)
        {
            return new Money(leftMoney.Amount + rightMoney);
        }

        public static Money operator -(Money leftMoney, Money rightMoney)
        {
            return new Money(leftMoney.Amount - rightMoney.Amount);
        }

        public static Money operator -(Money leftMoney, decimal rightMoney)
        {
            return new Money(leftMoney.Amount - rightMoney);
        }

        public static Money operator -(Money money)
        {
            return new Money(-money.Amount);
        }

        public static Money operator *(Money leftMoney, Money rightMoney)
        {
            return new Money(leftMoney.Amount * rightMoney.Amount);
        }

        public static Money operator *(Money leftMoney, decimal rightMoney)
        {
            return new Money(leftMoney.Amount * rightMoney);
        }

        public static Money operator /(Money leftMoney, Money rightMoney)
        {
            return new Money(leftMoney.Amount / rightMoney.Amount);
        }

        public static Money operator /(Money leftMoney, decimal rightMoney)
        {
            return new Money(leftMoney.Amount / rightMoney);
        }


        public static bool operator <(Money leftMoney, Money rightMoney)
        {
            return leftMoney.Amount < rightMoney.Amount;
        }

        public static bool operator >(Money leftMoney, Money rightMoney)
        {
            return leftMoney.Amount > rightMoney.Amount;
        }

        public static bool operator >=(Money leftMoney, Money rightMoney)
        {
            return leftMoney.Amount >= rightMoney.Amount;
        }

        public static bool operator <=(Money leftMoney, Money rightMoney)
        {
            return leftMoney.Amount <= rightMoney.Amount;
        }
    }
}