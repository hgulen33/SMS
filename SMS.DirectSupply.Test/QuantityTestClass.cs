﻿namespace SMS.DirectSupply.Test
{
    using System;

    using FluentAssertions;

    using NUnit.Framework;

    using Domain;

    [TestFixture]
    public class QuantityTestClass
    {
        [Test]
        public void QuantitySumbyQuantity()
        {
            var q1 = new Quantity(100, new Unit("m")); 
            var q2 = new Quantity(200, new Unit("kg"));
            Action act = () =>
                {
                    var quantity = (q1 + q2);
                };
            act.ShouldThrow<UnitsNotSameException>()
               .Which.Message.Should()
               .Be("Birimler aynı değil");

        }

        [Test]
        public void QuantityMinusByQuantity()
        {
            var q1 = new Quantity(100, new Unit("m"));
            var q2 = new Quantity(200, new Unit("kg"));
            Action act = () =>
            {
                var quantity = (q1 - q2);
            };
            act.ShouldThrow<UnitsNotSameException>()
               .Which.Message.Should()
               .Be("Birimler aynı değil");

        }
        [Test]
        public void QuantityMinusByDouble()
        {
            var q1 = new Quantity(100, new Unit("m"));
            const int Q2 = 200;
            Action act = () =>
            {
                var quantity = (q1.Substract(Q2));
            };
            act.ShouldThrow<QuantityValueExeption>();

        }
    }
}